# Apple Device Support

Device Support images are no longer used by Xcode 15.

> Copying files from a newer version of Xcode to an older version of Xcode for any purpose was never supported. That it worked for your needs was coincidental.
>
> (Souce: [Jason from Apple](https://developer.apple.com/forums/thread/730947))

This repository will be archived.
